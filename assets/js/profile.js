let token = localStorage.getItem("token");
// console.log(token);

let profileContainer = document.querySelector("#profileContainer");

if(!token || token === null) {

	// Unauthenticated user
	alert('You must login first');
	// Redirect user to login page
	window.location.href="./login.html";

} else {

	fetch('https://lit-dawn-14080.herokuapp.com/api/users/details', {
		method: 'GET',
		headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
	})
	.then(res => res.json())
	.then(data => {

		console.log(data);

		profileContainer.innerHTML = 
			`
				<div class="col-md-12">
					<section class="jumbotron my-5">		
						<h1 class="text-center">${data.firstName} ${data.lastName}</h1>
						<p class="text-center">${data.email}</p>
						<p class="text-center">${data.mobileNo}</p>
						<h3 class="text-center mt-5">Class History</h3>
						<div id="courses"></div> 

					</section>
				</div>
			`

		let courses = document.querySelector("#courses");

		if(data.enrollments.length > 0) {

			data.enrollments.forEach(courseData => {

				console.log(courseData);

				fetch(`https://lit-dawn-14080.herokuapp.com/api/courses/${courseData.courseId}`)
				.then(res => res.json())
				.then(data => {

					courses.innerHTML += 
						`
							<div class="card">
								<div class="card-body">
									<h1 class="card-text text-center">${data.name}</h1>
									<h3 class="card-text text-center">${courseData.enrolledOn}</h3>
									<h3 class="card-text text-center">${courseData.status}</h3>
								</div>
							</div>
						`

				})
			
			})

		} else {

			courses.innerHTML = `<h1 class="text-center">No courses available</h1>`

		}
		

	})
}