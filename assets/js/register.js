// selects the register form
let registerForm = document.querySelector("#registerUser");

// add an event to the register form
registerForm.addEventListener("submit", (e) => {

	// prevents page redirection/reload
	e.preventDefault();

	let firstName = document.querySelector("#firstName").value
	let lastName = document.querySelector("#lastName").value
	let mobileNumber = document.querySelector("#mobileNumber").value;
	let email = document.querySelector("#userEmail").value;
	let password1 = document.querySelector("#password1").value;
	let password2 = document.querySelector("#password2").value;

	if ((password1 !== '' && password2 !== '') && (password1 === password2) && (mobileNumber.length === 11)) {

		firstName = firstName.charAt(0).toUpperCase() + firstName.slice(1);
		lastName = lastName.charAt(0).toUpperCase() + lastName.slice(1);

		// fetch('url', {options})
		fetch('https://lit-dawn-14080.herokuapp.com/api/users/email-exists', {
			method: 'POST',
			headers: {
				'Content-Type': "application/json"
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data);

			if (data === false) {

				fetch('https://lit-dawn-14080.herokuapp.com/api/users', {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						password: password1,
						mobileNo: mobileNumber
					})
				})
				.then(res => res.json())
				.then(data => {

					console.log(data);

					if (data === true) {

						alert("Registered Successfully");

						// Redirect to login
						window.location.replace("./login.html");

					} else {

						alert("Something went wrong.")

					}

				})


			} else {

				alert("Duplicate email found.");

			}

		})

	}
})

// "{ email: email }"