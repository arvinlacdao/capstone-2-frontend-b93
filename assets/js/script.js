let navItem1 = document.querySelector("#navItem1");
let navItem2 = document.querySelector("#navItem2");

let userToken = localStorage.getItem("token");
console.log(userToken);

if (!userToken) {

	navItem1.innerHTML = 
		`
			<li class="nav-item">
				<a href="./login.html" class="nav-link"> Log In </a>
			</li>
		`

	navItem2.innerHTML =
		`
			<li class="nav-item">
				<a href="./register.html" class="nav-link"> Register </a>
			</li>
		`

} else {

	navItem1.innerHTML = 
		`
			<li class="nav-item">
				<a href="./profile.html" class="nav-link"> Profile </a>
			</li>
		`

	navItem2.innerHTML = 
		`
			<li class="nav-item">
				<a href="./logout.html" class="nav-link"> Logout </a>
			</li>
		`

}